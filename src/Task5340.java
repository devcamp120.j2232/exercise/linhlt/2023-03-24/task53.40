import s50.InvoiceItem;
public class Task5340 {
    public static void main(String[] args) throws Exception {
        InvoiceItem invoiceItem1 = new InvoiceItem("10001", "Laptop", 10, 9000000);
        InvoiceItem invoiceItem2 = new InvoiceItem("10002", "Monitor", 3, 5000000);
        System.out.println("Invoice 1: " + invoiceItem1);
        System.out.println("Invoice 2: " + invoiceItem2);
        System.out.println("Invoice 1 amount: " + invoiceItem1.getTotal());
        System.out.println("Invoice 2 amount: " + invoiceItem2.getTotal());
    }
}
